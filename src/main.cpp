#include "boost/filesystem.hpp"
#include "geometry.h"
#include "projectloader.h"
#include "projectsettings.h"
#include "stlreader.h"
#include "utils.h"
#include "vdbiface.h"
#include "writer.h"
#include <cstdint>
#include <fstream>
#include <gflags/gflags.h>
#include <glog/logging.h>
#include <iostream>
#include <limits>
#include <openvdb/math/Vec3.h>
#include <openvdb/openvdb.h>
#include <string>

// To run program with all features implemented below used:
//--dx=0,1 --Ny=12 --minloglevel=0 --num_cookies=11 --vmodule=stlreader=2 --v=0

namespace bfs = boost::filesystem;
int main(int argc, char* argv[])
{
    gflags::ParseCommandLineFlags(&argc, &argv, true);
    utils::setGoogleLoggerOpt(argv[0]);
    ProjectSettings project;

    if (argc == 1 || std::string(argv[1]).find("yaml") == std::string::npos) {
        std::string logfileName = bfs::current_path().string() + "/running_error";
        if (bfs::exists(logfileName + ".log"))
            bfs::remove(logfileName + ".log");
        google::SetLogDestination(google::GLOG_INFO, logfileName.c_str());
        LOG(INFO) << "Log file written to: " << logfileName.c_str();
        iLOG(FATAL) << "Expected YAML projectfile.";
    } else {
        ProjectLoader projectLoader(argv[1]);
        projectLoader.readProject(project);
    }

    std::string geomName = project.getGeometryList().front();
    aap::StlReader myReader(geomName);
    aap::Geometry myGeom;
    myReader.readFile(myGeom);
    //    std::cout << myGeom << std::endl;
    //    openvdb::math::Vec3d pA(0.5, 0.5, 1);
    //    openvdb::math::Vec3d pB(0.5, 0.5, -1);
    //    myGeom.testSegmentTriangleIntersection(pA, pB);
    //    myGeom.traslateBody(project.getDomain()->getDx() / 2., project.getDomain()->getDx() / 2., project.getDomain()->getDx() / 2.);
    std::cout << myGeom << std::endl;
    for (uint k = 0; k < project.getDomain()->sizeZ(); k++)
        for (uint j = 0; j < project.getDomain()->sizeY(); j++)
            for (uint i = 0; i < project.getDomain()->sizeX(); i++) {
                openvdb::Vec3d pA = project.getDomain()->transformIdxToWorld(i, j, k);
                for (int kk = -1; kk < 2; kk++)
                    for (int jj = -1; jj < 2; jj++)
                        for (int ii = -1; ii < 2; ii++) {
                            if (project.getDomain()->insideDomain(i + ii, j + jj, k + kk)) {
                                openvdb::math::Vec3d pB = project.getDomain()->transformIdxToWorld(i + ii, j + jj, k + kk);
                                //                                iLOG(INFO) << pA << " " << pB;
                                myGeom.testSegmentTriangleIntersection(pA, pB);
                            }
                        }
            }

    const auto gtransform = openvdb::math::Transform::createLinearTransform(project.getDomain()->getDx());
    const MeshDataAdapter<double> mesh = MeshDataAdapter<double>(
        myGeom.getPtrToVertexPos(), myGeom.getNumTriangles(), project.getDomain()->getDx());

    openvdb::DoubleGrid::Ptr bodyGrid = openvdb::tools::meshToVolume<openvdb::DoubleGrid>(
        mesh, *gtransform.get(), 0.f, std::numeric_limits<float>::max(), 0);

    for (openvdb::DoubleGrid::ValueOnCIter iter = bodyGrid->cbeginValueOn(); iter;
         ++iter) {
        DVLOG(1) << "Grid" << iter.getCoord() << "  "
                 << gtransform->indexToWorld(iter.getCoord()) << "  "
                 << "  sdf =" << *iter
                 << std::endl;
    }
    Writer domainWriter;
    domainWriter.writeVDBgridToVTU(project.getResultsFile(), bodyGrid);
    return 0;
}

// to compute segment-triangle 3D intersection:
//http://geomalgorithms.com/a06-_intersect-2.html
