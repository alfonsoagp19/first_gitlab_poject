#include "domain.h"
#include "utils.h"

Domain::Domain(double lx_, double ly_, double lz_, double dx_)
    : Lx(lx_)
    , Ly(ly_)
    , Lz(lz_)
    , dx(dx_)
{
    CHECK_NE(dx, 0.) << ": Grid spacing cannot be 0.0 meters.";
    Nx = Lx / dx + 1;
    Ny = Ly / dx + 1;
    Nz = Lz / dx + 1;
    LOG(INFO) << "Domain Size [" << Lx << ", " << Ly << ", " << Ly << "] meters.";
    LOG(INFO) << "Domain Size [" << Nx << ", " << Ny << ", " << Ny << "] number of points.";
    LOG(INFO) << "Grid spacing " << dx << " meters.";
}
double Domain::getDx() const
{
    return dx;
}

void Domain::setDx(double value)
{
    if (value != dx) {
        CHECK_NE(value, 0.) << ": Grid spacing cannot be 0.0 meters.";
        dx = value;
        Nx = Lx / dx;
        Ny = Ly / dx;
        Nz = Lz / dx;
    }
}
