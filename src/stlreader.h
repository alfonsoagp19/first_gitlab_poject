#ifndef STLREADER_H
#define STLREADER_H
#include "geometry.h"
#include <string>

namespace aap {

class StlReader {
public:
  StlReader(std::string file);
  void readFile(Geometry &geom);

private:
  std::string fileName_;
};
} // namespace aap
#endif // STLREADER_H
