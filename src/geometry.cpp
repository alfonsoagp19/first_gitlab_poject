#include "geometry.h"

namespace aap {
void Geometry::addTriangle(Triangle &tri) { triangles.push_back(tri); }
const Triangle &Geometry::getTriangle(uint triId) const { return triangles[triId]; }
void Geometry::addVertexPos(double x, double y, double z) {
  vertexPos.push_back(openvdb::math::Vec3d(x, y, z));
}
openvdb::math::Vec3d const &Geometry::getVertexPos(uint triId) const { return vertexPos[triId]; }

std::string Geometry::getName() const { return name; }

void Geometry::setName(const std::string &name_) { name = name_; }

void Geometry::traslateBody(double tx, double ty, double tz) {
  for (auto &vertex : vertexPos) {
    vertex += openvdb::math::Vec3d(tx, ty, tz);
  }
}

bool Geometry::testSegmentPlaneIntersection(const openvdb::math::Vec3d &p0,
                                            const openvdb::math::Vec3d &p1) const {
  return false;
}
bool Geometry::testSegmentTriangleIntersection(const openvdb::math::Vec3d &p0,
                                               const openvdb::math::Vec3d &p1) const {
  for (auto &tri : triangles) {
    openvdb::math::Vec3d normal = tri.getNormal();
    openvdb::math::Vec3d v0 = vertexPos[tri.vertexId_[0]];
    double denominator = normal.dot(p1 - p0);
    double numerator = normal.dot(v0 - p0);
    //        iLOG(INFO) << denominator << " " << normal << " " << p1 - p0;
    if (std::fabs(denominator) < utils::SMALL_DOUBLE) // segment parallel to triangle plane
    {
      if (numerator == 0) {
        DVLOG(3) << "Segment " << p0 << " " << p1 << " lies in plane defined by p0=" << (p1 - p0)
                 << " n=" << normal;
        return false;
      } else {
        DVLOG(3) << "Segment " << p0 << " " << p1 << " is parallel to plane with p0=" << (p1 - p0)
                 << " n=" << normal;
        return false;
      }
    }

    double rI = numerator / denominator;
    if (rI < 0. || rI > 1.) {
      DVLOG(3) << "Segment " << p0 << " " << p1 << " goes away from plane with p0=" << (p1 - p0)
               << " n=" << normal;
      return false;
    }

    openvdb::math::Vec3d pI = p0 + rI * (p1 - p0);
    openvdb::math::Vec3d u = vertexPos[tri.vertexId_[1]] - v0;
    openvdb::math::Vec3d v = vertexPos[tri.vertexId_[2]] - v0;
    openvdb::math::Vec3d w = pI - v0;

    double denominator2 = u.dot(v) * u.dot(v) - u.dot(u) * v.dot(v);
    double sI = (u.dot(v) * w.dot(v) - v.dot(v) * w.dot(u)) / denominator2;
    if (sI < 0. || sI > 1.) {
      DVLOG(3) << "Intersection between :" << p0 << " " << p1 << " and plane" << (p1 - p0)
               << " n=" << normal << "outside of triangle " << tri.id_;
      return false;
    }
    double tI = (u.dot(v) * w.dot(u) - u.dot(u) * w.dot(v)) / denominator2;
    if (tI < 0. || (sI + tI) > 1.) {
      DVLOG(3) << "Intersection between :" << p0 << " " << p1 << " and plane" << (p1 - p0)
               << " n=" << normal << "outside of triangle " << tri.id_;
      return false;
    } else {
      iLOG(INFO) << "Intersection FOUND between :" << p0 << " " << p1 << " and " << tri.id_
                 << " at " << pI << "\n";
      return true;
    }
  }
  return false;
}
std::ostream &operator<<(std::ostream &out, const Geometry &g) {
  out << "Geometry " << g.name << " \n";
  for (auto &tri : g.triangles) {
    out << tri << " \t";
    out << "vp1 " << g.vertexPos[tri.vertexId_[0]] << " \n\t"
        << "vp2 " << g.vertexPos[tri.vertexId_[1]] << " \n\t"
        << "vp3 " << g.vertexPos[tri.vertexId_[2]] << " \n";
  }
  return out;
}

} // namespace aap
