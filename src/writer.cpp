#include "writer.h"
#include <vtkSTLWriter.h>
#include <vtkSmartPointer.h>

#include <vtkCellArray.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkHexahedron.h>
#include <vtkPointData.h>
#include <vtkRectilinearGrid.h>
#include <vtkRectilinearGridWriter.h>
#include <vtkSmartPointer.h>
#include <vtkXMLUnstructuredGridWriter.h>
Writer::Writer() {}

void Writer::writeVDBgridToVTU(std::string filename, openvdb::DoubleGrid::Ptr domain)
{
    openvdb::Coord maxDim = domain->evalActiveVoxelDim();

    vtkDoubleArray* xCoords = vtkDoubleArray::New();
    vtkDoubleArray* yCoords = vtkDoubleArray::New();
    vtkDoubleArray* zCoords = vtkDoubleArray::New();
    vtkFloatArray* scalars = vtkFloatArray::New();
    vtkFloatArray* vel = vtkFloatArray::New();
    scalars->SetName("pressure");
    vel->SetName("velocity");
    vel->SetNumberOfComponents(3);

    for (int count = 0; count < maxDim.x(); count++) {
        xCoords->InsertValue(count, count);
        yCoords->InsertValue(count, count);
        zCoords->InsertValue(count, count);
    }
    int maxDim3 = maxDim.x() * maxDim.x() * maxDim.x();
    for (int count = 0; count < maxDim3; count++) {

        scalars->InsertValue(count, count * count);
        vel->InsertTuple3(count, count, count, count * count);
    }
    vtkSmartPointer<vtkRectilinearGrid> grid = vtkSmartPointer<vtkRectilinearGrid>::New();
    grid->SetXCoordinates(xCoords);
    grid->SetYCoordinates(yCoords);
    grid->SetZCoordinates(zCoords);
    grid->SetDimensions(maxDim.x(), maxDim.y(), maxDim.z());
    grid->GetPointData()->SetScalars(scalars);
    grid->GetPointData()->SetVectors(vel);

    // Write file
    vtkSmartPointer<vtkRectilinearGridWriter> writer = vtkSmartPointer<vtkRectilinearGridWriter>::New();
    filename += ".vtk";
    writer->SetFileName(filename.c_str());
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput(grid);
#else
    writer->SetInputData(grid);
#endif
    writer->Write();
}

void Writer::writeGeomToStl(aap::Geometry g)
{
    vtkSmartPointer<vtkSTLWriter> vtkStlwriter = vtkSTLWriter::New();
}
