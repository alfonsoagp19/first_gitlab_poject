#ifndef UTILS_H
#define UTILS_H
#include <gflags/gflags.h>
#include <glog/logging.h>

#define iLOG(a) LOG(a) << #a << ": "

namespace utils {
constexpr double SMALL_DOUBLE = 1e-15;
void setGoogleLoggerOpt(char* argv0);
}

#endif // UTILS_H
