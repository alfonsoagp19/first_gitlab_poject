#include "projectloader.h"

#include "projectsettings.h"

#include <list>
#include <yaml-cpp/yaml.h>

DECLARE_double(dx);

ProjectLoader::ProjectLoader(std::string file)
    : projFile(file)
{
    if (bfs::exists(projFile)) {
        if (bfs::is_regular_file(projFile)) {
            std::string logfileName = boost::filesystem::current_path().string() + "/" + projFile.stem().string();
            if (boost::filesystem::exists(logfileName + ".log"))
                boost::filesystem::remove(logfileName + ".log");
            google::SetLogDestination(google::GLOG_INFO, logfileName.c_str());
            LOG(INFO) << "Log file written to: " << logfileName.c_str();
            config = YAML::LoadFile(file);
        } else if (bfs::is_directory(projFile)) // is path p a directory?
        {
            DLOG(INFO) << file << " is a directory\n";
        } else
            DLOG(INFO) << file << " exists, but is not a regular file or directory\n";
    } else
        iLOG(ERROR) << file << " not found.";
}

bool ProjectLoader::readProject(ProjectSettings& settings)
{
    settings.setProjectName(projFile.stem().string());
    if (config["geometries"]) {
        YAML::Node geoms = config["geometries"];
        for (YAML::const_iterator it = geoms.begin(); it != geoms.end(); ++it) {
            VLOG(2) << "Geometry in yaml list: " << it->as<std::string>();
            settings.insertGeomFile(it->as<std::string>());
        }
        for (auto file : settings.getGeometryList())
            VLOG(1) << "Geometry file : " << file;
    }

    if (config["grid spacing"] && config["domain size"]) {
        VLOG(2) << "Grid Spacing in yaml list: " << config["grid spacing"].as<double>();
        YAML::Node domSize = config["domain size"];
        try {
            std::string dummy;
            double dx = 0.;
            //TODO: fix to be able to fall back for dx
            //            if (gflags::GetCommandLineOption("dx", &dummy)) {
            //                dx = FLAGS_dx;
            //                iLOG(WARNING) << "Overriding dx from command line input dx=" << FLAGS_dx << " " << dummy;
            //            } else
            {
                dx = config["grid spacing"].as<double>();
            }
            settings.initializeDomain(domSize[0].as<double>(), domSize[1].as<double>(), domSize[2].as<double>(),
                dx);
        } catch (YAML::TypedBadConversion<double>) {

            iLOG(FATAL) << "Domain variables Lx=" << domSize[0] << " Ly= " << domSize[1] << " Lz= " << domSize[2]
                        << " dx= " << config["grid spacing"] << " expected in floating point precission numbers.";
        }

    } else if (!config["grid spacing"])
        iLOG(FATAL) << "Missing grid spacing project file";
    else if (!config["domain size"])
        iLOG(FATAL) << "Missing Domain Size in YAML project file";
    return true;
}
