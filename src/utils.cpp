#include "utils.h"

//void utils::setGoogleLoggerOpt(char* argv0)
DEFINE_bool(mybool, true, "Include 'advanced' options in the menu listing");
DEFINE_string(projFile, "", "projectFile");
DEFINE_string(geomFile, "", "geometryFile");
DEFINE_double(dx, 0.1, "Grid Spacing in x-dir");
DEFINE_int64(Ny, 10, "Size in y-dir");

void utils::setGoogleLoggerOpt(char* argv0)
{
    FLAGS_timestamp_in_logfile_name = false;
    FLAGS_log_prefix = false;
    FLAGS_alsologtostderr = true;
    FLAGS_colorlogtostderr = true;
    FLAGS_stderrthreshold = 0;
    google::InitGoogleLogging(argv0);
    google::SetLogFilenameExtension(".log");
}
