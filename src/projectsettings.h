#ifndef PROJECTSETTINGS_H
#define PROJECTSETTINGS_H
#include "domain.h"
#include "utils.h"

#include <list>
#include <string>

class ProjectSettings {
public:
    ProjectSettings();
    ~ProjectSettings()
    {
        delete domain;
    }
    std::string getProjectName() const;
    void setProjectName(const std::string& value);

    std::list<std::string> getGeometryList() const;
    void setGeometryList(const std::list<std::string>& value);
    void insertGeomFile(const std::string& value);

    std::string getResultsFile() const;
    void setResultsFile(const std::string& value);
    void initializeDomain(double lx_, double ly_, double lz_, double dx_)
    {
        domain = new Domain(lx_, ly_, lz_, dx_);
    }
    Domain const* getDomain() { return domain; }

private:
    std::string projectName;
    std::list<std::string> geomtryFiles;
    std::string resultsFile;
    Domain* domain;
    //domain vars
};

#endif // PROJECTSETTINGS_H
