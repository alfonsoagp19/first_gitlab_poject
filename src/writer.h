#ifndef WRITER_H
#define WRITER_H

#include "geometry.h"

#include <openvdb/openvdb.h>

class Writer {
public:
    Writer();
    void writeVDBgridToVTU(std::string filename, openvdb::DoubleGrid::Ptr domain);
    void writeGeomToStl(aap::Geometry g);
};

#endif // WRITER_H
