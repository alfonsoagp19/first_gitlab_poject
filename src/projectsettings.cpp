#include "projectsettings.h"

ProjectSettings::ProjectSettings()
{
}

std::string ProjectSettings::getProjectName() const
{
    return projectName;
}

void ProjectSettings::setProjectName(const std::string& value)
{
    projectName = value;
}

std::list<std::string> ProjectSettings::getGeometryList() const
{
    return geomtryFiles;
}

void ProjectSettings::setGeometryList(const std::list<std::string>& value)
{
    geomtryFiles = value;
}

void ProjectSettings::insertGeomFile(const std::string& value)
{
    geomtryFiles.push_back(value);
}

std::string ProjectSettings::getResultsFile() const
{
    return resultsFile;
}

void ProjectSettings::setResultsFile(const std::string& value)
{
    resultsFile = value;
}
