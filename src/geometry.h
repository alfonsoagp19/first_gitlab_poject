#ifndef GEOMETRY_H
#define GEOMETRY_H
#include "vdbiface.h"
#include <openvdb/math/Vec3.h>
#include <vector>
namespace aap {

struct Triangle {
    Triangle() {}
    Triangle(uint id)
        : id_(id)
    {
        vertexId_ = openvdb::math::Vec3ui(id * 3, id * 3 + 1, id * 3 + 2);
    }
    Triangle(uint id, openvdb::math::Vec3d normal)
        : id_(id)
        , normal_(normal)
    {
        vertexId_ = openvdb::math::Vec3ui(id * 3, id * 3 + 1, id * 3 + 2);
    }
    Triangle(uint id, openvdb::math::Vec3ui vertexId, openvdb::math::Vec3d normal)
        : id_(id)
        , vertexId_(vertexId)
        , normal_(normal)
    {
    }

    openvdb::math::Vec3ui getVertexId() const { return vertexId_; }
    void setVertexId(const openvdb::math::Vec3ui& vertexId)
    {
        vertexId_ = vertexId;
    }
    openvdb::math::Vec3d getNormal() const { return normal_; }
    void setNormal(const openvdb::math::Vec3d& normal) { normal_ = normal; }

    friend std::ostream& operator<<(std::ostream& out, const Triangle& tri)
    {
        out << "Triangle " << tri.id_ << ": normal " << tri.normal_ << "\n\t vIds "
            << tri.vertexId_ << "\n";
        return out;
    }

    // DATA
    uint id_ = -1;
    openvdb::math::Vec3ui vertexId_;
    openvdb::math::Vec3d normal_;
};

class Geometry {
public:
    Geometry() {}
    Triangle const* getPtrTriangles() const { return &triangles[0]; }
    size_t getNumTriangles() const { return triangles.size(); }
    void addTriangle(Triangle& tri);
    Triangle const& getTriangle(uint triId) const;
    openvdb::math::Vec3d* getPtrToVertexPos() { return &vertexPos[0]; }
    openvdb::math::Vec3d const* getPtrToVertexPosCnst() const { return &vertexPos[0]; }
    size_t getNumVertexPos() const { return vertexPos.size(); }
    void addVertexPos(double x, double y, double z);
    openvdb::math::Vec3d const& getVertexPos(uint triId) const;
    friend std::ostream& operator<<(std::ostream& out, const Geometry& g);
    std::string getName() const;
    void setName(const std::string& name_);
    void createBodyGri();
    void traslateBody(double tx, double ty, double tz);
    bool testSegmentPlaneIntersection(const openvdb::math::Vec3d& p0, const openvdb::math::Vec3d& p1) const;
    bool testSegmentTriangleIntersection(const openvdb::math::Vec3d& p0, const openvdb::math::Vec3d& p1) const;
    // DATA
private:
    std::string name;
    std::vector<Triangle> triangles;
    std::vector<openvdb::math::Vec3d> vertexPos;
};
} // namespace aap
#endif // GEOMETRY_H
