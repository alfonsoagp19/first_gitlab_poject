#ifndef DOMAIN_H
#define DOMAIN_H
#include <openvdb/math/Vec3.h>

class Domain {
public:
    Domain() {}
    Domain(double lx_, double ly_, double lz_, double dx_);
    double getDx() const;
    void setDx(double value);
    inline uint lenghtX() const { return Lx; }
    inline uint lenghtY() const { return Ly; }
    inline uint lenghtZ() const { return Lz; }
    inline uint sizeX() const { return Nx; }
    inline uint sizeY() const { return Ny; }
    inline uint sizeZ() const { return Nz; }
    inline openvdb::math::Vec3d transformIdxToWorld(uint i, uint j, uint k) const
    {
        assert(i >= 0);
        assert(i < Nx);
        assert(j >= 0);
        assert(j < Ny);
        assert(k >= 0);
        assert(k < Nz);

        return openvdb::math::Vec3d(i * dx, j * dx, k * dx);
    }
    openvdb::math::Vec3d transformIdxToWorld(const openvdb::math::Vec3ui& idx) const
    {
        return transformIdxToWorld(idx.x(), idx.y(), idx.z());
    }
    openvdb::math::Vec3ui transformWorldToIdx(double px, double py, double pz) const
    {
        assert(px >= 0);
        assert(px < Lx);
        assert(py >= 0);
        assert(py < Ly);
        assert(pz >= 0);
        assert(pz < Lz);

        return openvdb::math::Vec3ui(px / dx, py / dx, pz / dx);
    }
    openvdb::math::Vec3ui transformWorldToIdx(const openvdb::math::Vec3d& pos) const
    {
        return transformWorldToIdx(pos.x(), pos.y(), pos.z());
    }
    inline bool insideDomain(uint i, uint j, uint k) const { return (i >= 0 && i < Nx && j >= 0 && j < Ny && k >= 0 && k < Nz); }
    //data
private:
    double Lx = 0., Ly = 0., Lz = 0.;
    unsigned int Nx = 0, Ny = 0, Nz = 0;
    double dx = 1.;
};

#endif // DOMAIN_H
