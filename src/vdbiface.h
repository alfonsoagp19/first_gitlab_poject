#ifndef VDBIFACE_H
#define VDBIFACE_H
#include "utils.h"
#include <openvdb/tools/MeshToVolume.h>

template <typename P>
struct MeshDataAdapter {
    MeshDataAdapter(openvdb::math::Vec3<P>* vertexPos, size_t numTriangles, P sc)
        : numTri(numTriangles)
        , vertices(vertexPos)
        , scaleFromGlobToLocIdx(1. / sc)
    {
        DVLOG(3) << numTriangles << " " << vertexPos << std::endl;
        for (size_t i = 0; i < numTri; i++)
            DVLOG(3) << "tri num " << i << " " << vertexPos[3 * i] << " "
                     << vertexPos[3 * i + 1] << " " << vertexPos[3 * i + 2] << " "
                     << std::endl;
    }
    size_t polygonCount() const // Total number of polygons
    {
        return numTri;
    }
    size_t pointCount() const // Total number of points
    {
        return numTri * 3;
    }
    size_t vertexCount([[maybe_unused]] size_t n) const // Vertex count for polygon n
    {
        return 3;
    }
    // Return position pos in local grid index space for polygon n and vertex v
    void getIndexSpacePoint(size_t n, size_t v, openvdb::Vec3d& pos) const
    {
        pos = vertices[3 * n + v] * scaleFromGlobToLocIdx;
    }

    // data
private:
    size_t numTri = 0;
    openvdb::math::Vec3<P>* vertices;
    P scaleFromGlobToLocIdx;
};

class VdbIface {
public:
    VdbIface();
};

#endif // VDBIFACE_H
