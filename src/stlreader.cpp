#include "stlreader.h"
#include "utils.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;
namespace aap {
StlReader::StlReader(std::string file)
    : fileName_(file)
{
}

void StlReader::readFile(aap::Geometry& geom)
{
    bfs::path p(fileName_); // avoid repeated path construction below

    if (bfs::exists(p)) // does path p actually exist?
    {
        bfs::ifstream inFile(p);
        if (bfs::is_regular_file(p) && p.extension().compare(".stl") == 0) // is path p a regular file?
        {
            std::string line;
            std::string geomName;
            std::getline(inFile, line);
            if (line.find("solid ") != -1) {
                geom.setName(line.substr(line.find_first_of(" ") + 1));
                LOG(INFO) << "Reading geomtry file: " << fileName_;
            }
            unsigned int numTri = 0;
            while (std::getline(inFile, line) && line.find("facet normal ") != -1) {
                DVLOG(3) << "reading triangle " << numTri << std::endl;
                std::vector<std::string> results;
                boost::split(
                    results,
                    line.substr(line.find("facet normal ") + sizeof("facet normal")),
                    [](char c) { return c == ' '; });
                openvdb::math::Vec3d normal = openvdb::math::Vec3d(std::stod(results[0]), std::stod(results[1]),
                    std::stod(results[2]));
                aap::Triangle triTemp(numTri, normal);
                geom.addTriangle(triTemp);
                std::getline(inFile, line);

                for (int i = 0; i < 3; i++) {
                    std::getline(inFile, line);
                    boost::split(results,
                        line.substr(line.find("vertex ") + sizeof("vertex")),
                        [](char c) { return c == ' '; });
                    geom.addVertexPos(std::stod(results[0]), std::stod(results[1]),
                        std::stod(results[2]));
                }
                numTri++;
                std::getline(inFile, line);
                std::getline(inFile, line);
            }
            DVLOG(2) << geom;
        } else if (bfs::is_directory(p)) // is path p a directory?
        {
            DLOG(INFO) << p << " is a directory\n";
        } else
            DLOG(INFO) << p << " exists, but is not a regular file or directory\n";
    } else
        iLOG(ERROR) << fileName_ << " not found.";
}
} // namespace aap
