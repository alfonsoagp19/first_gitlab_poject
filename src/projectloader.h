#ifndef PROJECTLOADER_H
#define PROJECTLOADER_H
#include "boost/filesystem.hpp"
#include "yaml-cpp/node/node.h"

#include <string>

namespace bfs = boost::filesystem;

class ProjectSettings;

class ProjectLoader {
public:
    ProjectLoader(std::string file);
    bool readProject(ProjectSettings& settings);

private:
    bfs::path projFile;
    YAML::Node config;
};

#endif // PROJECTLOADER_H
